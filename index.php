<?php

require_once __DIR__ . '/persistencia/bd.php';
require_once __DIR__ . '/controlador/sessao.php';


if (isset($_SERVER['PATH_INFO'])) {
    $caminho = $_SERVER['PATH_INFO'];
} else {
    $caminho = '/';
}

if ($caminho == '/') {
    require_once __DIR__ . '/controlador/home.php';
} else if ($caminho == '/lista_usuarios') {
    require_once __DIR__ . '/controlador/lista_usuarios.php';
} else if ($caminho == '/perfil_usuario') {
    require_once __DIR__ . '/controlador/perfil_usuario.php';
} else if ($caminho == '/editar_usuario') {
    require_once __DIR__ . '/controlador/editar_usuario.php';
} else if ($caminho == '/incluir_mensagem') {
    require_once __DIR__ . '/controlador/incluir_mensagem.php';
} else if ($caminho == '/criar_usuario') {
    require_once __DIR__ . '/controlador/criar_usuario.php';
} else if ($caminho == '/logout') {
    require_once __DIR__ . '/controlador/logout.php';
}
