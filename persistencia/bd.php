<?php

class Repositorio {
    function __construct() {
        $this->conexao = new PDO('mysql:dbname=robson;host=127.0.0.1;charset=UTF8',
                'robson', '209211');
        $this->conexao->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}


class RepositorioUsuarios extends Repositorio {    
    function listar_todos_usuarios() {
        return $this->conexao->query('select * from usuario order by nome')->fetchAll();
    }
    
    function pesquisa_por_nome($nome) {
        $consulta = $this->conexao->prepare('select * from usuario where nome like ? order by nome');

        $consulta->execute(["%$nome%"]);

        return $consulta->fetchAll();
    }

    function buscar_usuario($id) {
        $consulta = $this->conexao->prepare('select * from usuario where id = ?');

    //    $consulta->bindValue(1, $id);
    //    $consulta->execute();

        $consulta->execute([$id]);

    //    $lista_usuarios = $this->conexao->query('select * from usuario where id = ' . $id);
        return $consulta->fetch();
    }

    function buscar_por_login($login) {
        $consulta = $this->conexao->prepare('select * from usuario where login = ?');

        $consulta->execute([$login]);

        return $consulta->fetch();
    }
 
    function incluir_mensagem($mensagem) {
        $sql = 'insert into linha_tempo( texto, data, id_usuario)values(:mensagem, now(), :idusuario)';

        $consulta = $this->conexao->prepare($sql);

        $consulta->execute(['mensagem' => $mensagem['mensagem'], 'idusuario' => $mensagem['idusuario'] ]);
    }

    function listar_mensagens($id) {
        $sql = 'select id, texto, data, id_usuario from linha_tempo where id_usuario = :idusuario order by id desc';

        $consulta = $this->conexao->prepare($sql);
        $consulta->execute(['idusuario' => $id]);

        return $consulta->fetchAll();
    }
    
    
    function salvar_usuario($usuario) {
        $sql = 'update usuario
    set
        nome = :nome,
        cidade = :cidade,
        login = :login,
        senha = :senha
    where id = :id';
        $consulta = $this->conexao->prepare($sql);

        $consulta->execute($usuario);
    }

    function criar_usuario(/*.....*/) {
        // TRABALHO 03


        return $this->conexao->lastInsertId();
    }

}
