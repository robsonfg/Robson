<?php

$repo = new RepositorioUsuarios();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = [
        'id' => $_GET['id'],
        'nome' => $_POST['nome'],
        'cidade' => $_POST['cidade'],
        'login' => $_POST['login'],
        'senha' => $_POST['senha'],
    ];
    
    $repo->salvar_usuario($usuario);
}

$usuario = $repo->buscar_usuario($_GET['id']);

require_once __DIR__ . '/../interface/editar_usuario.php';
