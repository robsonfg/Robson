<?php

$repo = new RepositorioUsuarios();

if (isset($_REQUEST['nome'])) {
    $filtroNome = $_REQUEST['nome'];
    $lista_usuarios = $repo->pesquisa_por_nome($filtroNome);
} else {
    $lista_usuarios = $repo->listar_todos_usuarios();
}
//var_dump($lista_usuarios);

require_once __DIR__ . '/../interface/lista_usuarios.php';
//echo json_encode($lista_usuarios);
