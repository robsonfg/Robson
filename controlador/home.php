<?php

$repo = new RepositorioUsuarios();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = $repo->buscar_por_login($_POST['login']);

    $sucesso = ($usuario != null && $usuario['senha'] == $_POST['senha']);
    
    if ($sucesso) {
        $_SESSION['usuario_logado'] = $usuario;
        $usuarioLogado = $usuario;
    }
}

require_once __DIR__ . '/../interface/home.php';
