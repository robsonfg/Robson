<?php

$repo = new RepositorioUsuarios();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $mensagem = [
        'idusuario' => $_GET['id'],
        'mensagem' => $_POST['mensagem'],
    ];
    
    $repo->incluir_mensagem($mensagem);
}

$usuario = $repo->buscar_usuario($_GET['id']);
$lista_mensagem = $repo->listar_mensagens($_GET['id']);

require_once __DIR__ . '/../interface/perfil_usuario.php';
