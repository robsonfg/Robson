<?php require_once __DIR__ . '/cabecalho.php'; ?>
        
        <?php if ($usuario == null) { ?>
        <p>Usuário não encontrado</p>
        <?php } else { ?>
        
        <form method="POST" action="/face/index.php/editar_usuario?id=<?= $usuario['id'] ?>">
            <table>
                <tr>
                    <th>Login</th>
                    <td><input name="login" value="<?= $usuario['login'] ?>"></td>
                </tr>
                <tr>
                    <th>Senha</th>
                    <td><input name="senha" value="<?= $usuario['senha'] ?>"></td>
                </tr>
                <tr>
                    <th>Nome</th>
                    <td><input name="nome" value="<?= $usuario['nome'] ?>"></td>
                </tr>
                <tr>
                    <th>Cidade</th>
                    <td><input name="cidade" value="<?= $usuario['cidade'] ?>"></td>
                </tr>
            </table>

            <input type="submit" value="Salvar">
        </form>
        
        <?php } ?>
        
<?php require_once __DIR__ . '/rodape.php'; ?>
