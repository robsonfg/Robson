<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Face</title>
        <link href="/face/css/bootstrap.min.css" rel="stylesheet">
        <style>
            body {
                margin-top: 80px;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/face">Face</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="/face/index.php/lista_usuarios">Lista dos usuários</a></li>
            
        <?php if (isset($usuarioLogado)) { ?>
        <li>
            <a href="/face/index.php/perfil_usuario?id=<?= $usuarioLogado['id'] ?>">Usuário: <?= $usuarioLogado['nome'] ?></a>
        </li>
        <li>
            <a href="/face/index.php/logout">Logout</a>
        </li>
        <?php } else { ?>
        <li>
            <a href="/face/index.php/criar_usuario">Cadastre-se</a>
        </li>
        <?php } ?>

                    
              </ul>
            </div>
          </div>
        </nav>

        <div class="container">
