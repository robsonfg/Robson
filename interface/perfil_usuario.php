<?php require_once __DIR__ . '/cabecalho.php'; ?>

    <!--     <a href="/face/index.php/perfil_usuario?id=<?= $usuarioLogado['id'] ?>">Usuário: <?= $usuarioLogado['nome'] ?></a> -->
       <?php if ($usuario == null) { ?>
        <p>Usuário não encontrado</p>
        <?php } else { ?>
        <table>
            <tr>
                <th>Nome</th>
                <td><?= $usuario['nome'] ?></td>
            </tr>
            <tr>
                <th>Cidade</th>
                <td><?= $usuario['cidade'] ?></td>
            </tr>
        </table>
        <?php } ?>
        
        <?php if (isset($usuarioLogado) && $usuarioLogado['id'] == $usuario['id']) { ?>
        <a href="/face/index.php/editar_usuario?id=<?= $usuario['id'] ?>">Editar perfil</a>
        <form method="POST" action="/face/index.php/incluir_mensagem?id=<?= $usuario['id'] ?>"
            <div>
                <input name="mensagem" class="form-control" placeholder="O que você está pensando?"/>
                <input type="submit" value="Enviar" class="btn btn-primary"/>
            </div>
        </form>
        <?php } ?>
 
        <hr>
        
        
        <ul>
            <?php foreach ($lista_mensagem as $mensagem) { ?>
            <li>
              <div class="panel panel-default">
               <div class="panel-body">
                <?= $mensagem['texto']?> 
               </div>
               <?php
               $date = new DateTime($mensagem['data']); 
               ?>
                  <div class="panel-default">Enviada em <?= date_format($date, 'd/m/Y H:m:s')?></div>
               </div>
            </li>
            <?php } ?>
        </ul>
        
        
             
<?php require_once __DIR__ . '/rodape.php'; ?>
