<?php require_once __DIR__ . '/cabecalho.php'; ?>

        <?php if (isset($filtroNome)) { ?>
        <h2>Usuários cujo nome contem: <?= $filtroNome ?></h2>
        <?php } else { ?>
        <h2>Lista de todos os usuários</h2>
        <?php } ?>
        
        <ul>
            <?php foreach ($lista_usuarios as $usuario) { ?>
            <li>
                <a href="/face/index.php/perfil_usuario?id=<?= $usuario['id'] ?>">
                    <?= $usuario['nome'] . ' (' . $usuario['cidade'] . ')' ?>
                </a>
            </li>
            <?php } ?>
        </ul>
        
        <form action="/face/index.php/lista_usuarios" method="GET">
            <label>Pesquisa por nome</label>
            <input name="nome"/>
            <input type="submit" value="Filtrar"/>
        </form>
        
        <?php /*
        <!-- forma alternativa para fazer o mesmo -->
        <ul>
            <?php foreach ($lista_usuarios as $usuario) {
            echo '<li>' . $usuario['nome'] . ' (' . $usuario['cidade'] . ')</li>';
            } ?>
        </ul>
         */ ?>
        
<?php require_once __DIR__ . '/rodape.php'; ?>
